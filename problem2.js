const fs = require('fs');


function readFileData (filePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, "utf-8", (err, data) => {
            if (err) {
                reject(err);
            } else {
                // console.log(`${fileName} file data read successfully`);
                resolve(data)
            }
        });
    });
}

function writeFile(filePath, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(filePath, data, {'flag':'a'}, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve(`${filePath} New file created`)
            }
        })
    })
}

function appendFile(filePath, newFileName) {
    return new Promise((resolve, reject) => {
        fs.appendFile(filePath, newFileName, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve(`${newFileName} Created file appended to filenames.txt`)
            }
        })
    })
}

function convertDataToUppercase(newFilePath, data){
    return new Promise((resolve, reject) => {
        let upperCaseData = data.toUpperCase();
        writeFile(newFilePath, upperCaseData)
            .then((data) => {
                console.log(data)
                resolve("Data converted to uppercase")
                appendFile("filenames.txt", `${newFilePath} `)
                    .then((data) => console.log(data))
                    .catch((err) => console.log(err))
            }, (err) => reject(err))
        
    })
}

function convertToLowerCaseAndSplitSentences(filePath, newFileName){
    return new Promise((resolve, reject) => {
        readFileData(filePath)
            .then((data) => {
                let splittedContent = data.toLowerCase().split(".");
                for (let i=0; i<splittedContent.length; i++) {
                    writeFile(newFileName, `${splittedContent[i]}\n`)
                        .then((data) => resolve(data))
                        .catch((err) => reject(err))
                }
                appendFile("filenames.txt", `${newFileName} `)
                    .then((data) => console.log(data))
                    .catch((err) => console.log(err))   
            })
            .catch((err) => reject(err))           
    })            
}


function readFilesAndSortContent(filePath, newFileName){
    return new Promise((resolve, reject) => {
        readFileData(filePath)
            .then((data) => {
                resolve(data)
                let sortedData = data.split("\n").sort((a, b) =>{
                    return a.length - b.length;
                })
                for (let i=0; i<sortedData.length; i++) {
                    writeFile(newFileName, `${sortedData[i]}\n`)
                        .then((data) => resolve(data))
                        .catch((err) => reject(err))
                }
                appendFile("filenames.txt", `${newFileName} `)
                    .then((data) => console.log(data))
                    .catch((err) => console.log(err))   
            })
            .catch((err) => reject(err))
    })
}

function deleteFile(fileNames) {
    return new Promise((resolve, reject) => {
        let files = fileNames.split(" ")
        files.forEach((filePath) => {
            if (filePath != ""){
                fs.unlink(filePath, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve("All files deleted")
                    }
                })
            }
            
        })
    })
}

function readFileAndDeleteContent(filePath) {
    return new Promise((resolve, reject) => {
        readFileData(filePath)
            .then((data) => {
                deleteFile(data)
                    .then((data) => resolve(data))
                    .catch((err) => reject(err))
            })
            .catch((err) => reject(err))
    })
}

module.exports= {readFileData, convertDataToUppercase, convertToLowerCaseAndSplitSentences, readFilesAndSortContent, readFileAndDeleteContent};

