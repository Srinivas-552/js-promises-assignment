const {createFolder, createJsonFile, deleteJsonFile} = require('../problem1');

const path = require("path")

let folderPath = path.resolve(__dirname, '../JSONFiles')
// let file1 = path.resolve(__dirname, '../JSONFiles/file1.json')
// let file1 = path.resolve(__dirname, '../JSONFiles/file1.json')


createFolder('JSONFiles')
    .then((data) => {
        console.log(data)
        return createJsonFile(folderPath, 3)
    })
    .then((fileNameArr) => {
        console.log("Created new files")
        return deleteJsonFile(fileNameArr)
    })
    .then((data) => {
        console.log(data)
    })
    .catch((err) => console.log(err))




