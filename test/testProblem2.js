const {readFileData, convertDataToUppercase, convertToLowerCaseAndSplitSentences, readFilesAndSortContent, readFileAndDeleteContent} = require('../problem2');



readFileData("lipsum.txt")
    .then((data) => {
        console.log("File data read successfully")
        return convertDataToUppercase("uppercaseData.txt", data)
    })
    .then((data) => {
        console.log(data)
        return convertToLowerCaseAndSplitSentences("uppercaseData.txt", "lowercaseData.txt")
    })
    .then((data) => {
        console.log(data)
        return readFilesAndSortContent("lowercaseData.txt", "sortedData.txt")
    })
    .then(() => {
        return readFileAndDeleteContent("filenames.txt")
    })
    .then((data) => console.log(data))
    .catch((err) => console.log(err))




